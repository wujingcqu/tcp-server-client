#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#define PORT 6666
#define MAXLINE 4096

int main(int argc, char** argv) {
    int sockfd, num, port;
    char buff[MAXLINE];
    struct hostent* he;
    struct sockaddr_in server;

    if (argc < 2) {
        printf("Usage: ./<IP Address>\n");
        exit(1);
    }

    he = gethostbyname(argv[1]);

    if (he == NULL) {
        printf("gethostbyname error\n");
        exit(1);
    }

    sockfd = socket(AF_INET, SOCK_STREAM, 0);

    if (sockfd == -1) {
        printf("socket error\n");
        exit(1);
    }

    if (argv[2]) {
        port = atoi(argv[2]);
        printf("socket port:%d\n", port);
    } else {
        port = PORT;
    }

    bzero(&server, sizeof(server));
    server.sin_family = AF_INET;
    server.sin_port = htons(port);
    server.sin_addr = *((struct in_addr*)he->h_addr);

    if (connect(sockfd, (struct sockaddr*)&server, sizeof(server)) == -1) {
        printf("connect error\n");
        exit(1);
    }

    do {
        printf("send msg to server: \n");
        fgets(buff, MAXLINE, stdin);

        if (!memcmp("quit", buff, 4)) {
            printf("quit server\n");
            break;
        }

        if (write(sockfd, buff, strlen(buff)) == -1) {
            printf("send msg error: %s \n", strerror(errno));
            exit(1);
        }

        memset(buff, 0, MAXLINE);
    } while (1);

    close(sockfd);
    return 0;
}
