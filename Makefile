
TARGETS = tcpserver tcpclient

CFLAGS = -Wall $(DEBUG) -I. $(INCLUDES) $(DEFINES) -Wno-unused-variable -D_FILE_OFFSET_BITS=64 -D__unix__
LDFLAGS =
DEPFLAGS =

all : $(TARGETS)

tcpserver : tcpserver.o
	$(CC) -o $@ $<

tcpserver.o : tcpserver.c
	$(CC) -c $(CFLAGS) $< -o $@

tcpclient : tcpclient.o
	$(CC) $(LDFLAGS) -o $@ $<

tcpclient.o : tcpclient.c
	$(CC) -c $(CFLAGS) $< -o $@

clean :
	rm tcpserver tcpclient tcpclient.o tcpserver.o
